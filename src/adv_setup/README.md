# TODO

## Scripts 

- GPS Offset Correction
- Thermal Regulation
- Multi-GPS Integration?

## Notes from proposal

- Utilizing the "offset" data outputted by the GPS receiver that corrects for the known error in when the PPS output will actually come (this has to do with the GPS chip itself not clocked at 1GHz and therefore it doesn't detect the "on-one-second" state until potentially after it's occurred when it wakes up while running on a 25-50MHz clock)

- Utilizing custom busy spin code on yet other (non critical cores) to actually generate heat as part of a "thermal regulation" of the air around the raspberry pi and GPS receiver. The more thermally stable the environment, theoretically, the better.


```
python heat.py
```

This should heat the RPi to around 50 C. You can change the target temperature by changing the parameter `targetTemp` in the script.

- Utilizing the PPS output from MANY GPS receivers, each connected to a single pin on the raspberry pi. We'd need to talk with U-Blox on details, but the intuition is that the earliest or some minimum number toggled would be treated as the signal rather than relying on only a single receiver. If nothing else, getting statistics on how different chips, all located nearby, would be quite interesting.
