#!/bin/bash

log_file="metrics.csv"
duration=600
high_frequency=5
medium_frequency=30
low_frequency=300

metric_get(){
    start_time=$(date +%s)

    while [ $(($(date +%s) - start_time)) -lt $duration ]; do    
        if (( $(date +%s) % $high_frequency == 0 )); then
            pps_stats=$(chronyc sourcestats -v | grep 'PPS' | awk '{print $2, $4}') 

            read offset jitter <<< $(echo $pps_stats)
            offset=$(printf "%.6f" $offset)
            jitter=$(printf "%.6f" $jitter)  
            timestamp=$(date +"%Y-%m-%d %H:%M:%S")
            echo "$timestamp, offset: $offset, jitter: $jitter" >> $log_file
        fi

        if (( $(date +%s) % $medium_frequency == 0 )); then
            temp=$(vcgencmd measure_temp | egrep -o '[0-9]*\.[0-9]*') 
            cpu_usage=$(top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk  '{print 100 - $1}')
            timestamp=$(date +"%Y-%m-%d %H:%M:%S")
            echo "$timestamp,temperature: $temp, CPU Usage: $cpu_usage%" >> $log_file
        fi 
        sleep 1    
    done
}

metric_get 
