# TODO 

## Scripts

- Installing dependencies
- Configuring GPSD
- Configuring Chrony 

## Notes from Proposal 

- isol_cpu kernel parameter so that all processes by default run on the 1st of the four raspberry pi cores (leaving the other three free from any processes or software by default)

- Pinning individual time-related processes each to their own dedicated core(s) (we will need to check the threading model to see if multi-threaded or not and if so, which threads are actually in the critical path for timing)

- Interrupt steering so that PPS output IRQ goes to either the core running whichever daemon handles the PPS (GPSD or chrony presumably) or another dedicated core
